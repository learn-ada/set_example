with Text_IO;
with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Example_Set;

use Ada.Integer_Text_IO;
use Ada.Text_IO;
use Text_IO;
use Example_Set;

procedure Task1 is
  counter      : integer := 0;
  input_symbol : Character := 'F';

  myset : Example_Set.Set;

begin

  Put_Line("Start");

  Clear(myset);

  Add(myset, 10);
  Add(myset, 11);
  Add(myset, 12);
  Add(myset, 13);

  Put_Line(Boolean'Image(
           Find(myset,10)
          ));

  Put_Line(Boolean'Image(
           Find(myset,1)
          ));

  Put_Line("Exit.");

end Task1;
