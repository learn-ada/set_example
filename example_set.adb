with Text_IO;
with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Interfaces.C;

use Ada.Integer_Text_IO;
use Ada.Text_IO;
use Text_IO;
use Interfaces.C;

package body Example_Set is

  procedure Clear(s : in out Set) is
  begin
    s.top := 0;
  end Clear;



  procedure Add(s : in out Set; e : Integer) is
  begin
    s.data(s.top) := e;
    s.top := s.top+1;
  end Add;

  procedure Del(s : in out Set; e : Integer) is
  begin
    null;
  end Del;


  function Find(s : Set; e : Integer) return Boolean is
    res : Boolean := false;
  begin

    for i in s.data'First .. s.data'Last loop

      if s.data(i) = e then
        res := True;
        exit;
      end if;

    end loop;

    return res;
  end Find;



end Example_Set;
