with Text_IO;
with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Interfaces.C;

package Example_Set is

  type Set is private;

  procedure Clear(s : in out Set);

  procedure Add(s : in out Set; e : Integer);
  procedure Del(s : in out Set; e : Integer);
  function Find(s : Set; e : Integer) return Boolean;


  private

  type DataStorage is array (0 .. 9) of Integer;

  type Set is record
    data : DataStorage;
    top  : Integer;
  end record;


end Example_Set;
